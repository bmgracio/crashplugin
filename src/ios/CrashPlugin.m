/********* PureePlugin.m Cordova Plugin Implementation *******/

#import <Cordova/CDV.h>
#import "CrashPlugin.h"
#import "OSLogger.h"

@interface CrashPlugin() 

@end

@implementation CrashPlugin

-(void) pluginInitialize {
    installUncaughtExceptionHandler();
}

void installUncaughtExceptionHandler() {
    NSSetUncaughtExceptionHandler(&exceptionHandler);
    signal(SIGABRT, &signalHandler);
    signal(SIGILL, &signalHandler);
    signal(SIGSEGV, &signalHandler);
    signal(SIGFPE, &signalHandler);
    signal(SIGBUS, &signalHandler);
    signal(SIGPIPE, &signalHandler);
}

void exceptionHandler(NSException *exception) {
    NSArray *stack = [exception callStackReturnAddresses];
    NSString *stack_message = [stack componentsJoinedByString:@"\n"];
    
    //NSLog(@"[EXCEPTION] Stack trace: %@", stack);
    
    [OSLogger logErrorWithMessage:@"" andWithModuleName:@"" andWithStack:stack_message];
}

void signalHandler(int signal) {
    //NSLog(@"[SIGNAL] Code: %d", signal);
    //NSLog(@"[SIGNAL] Stack trace: %@", [NSThread callStackSymbols]);
    
    NSArray *stack = [NSThread callStackSymbols];
    NSString *stack_message = [stack componentsJoinedByString:@"\n"];
    
    [OSLogger logErrorWithMessage:@"" andWithModuleName:@"" andWithStack:stack_message];
}

@end
